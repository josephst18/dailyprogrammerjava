import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Joseph on 10/22/2014.
 *
 * @author Joseph
 */
public class HandleGenerator {
    public static final String LISTPATH = "C:\\Users\\Joseph\\Source\\Repos\\DailyProgrammerJava\\C0185_Easy\\enable1.txt";
    public static void main(String[] args) throws FileNotFoundException {
        Scanner listScan = new Scanner(new File(LISTPATH));
        while (listScan.hasNextLine()) {
            String atString = listScan.next();
            if (atString.substring(0, 2).equals("at")) {
                    System.out.println("@" + atString.substring(2));
            }
            listScan.nextLine();
        }
    }
}
