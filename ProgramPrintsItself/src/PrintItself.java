import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Joseph on 8/9/2014.
 *
 * @author Joseph
 */
public class PrintItself {
    public final static String PATH = "C:\\Users\\Joseph\\Source\\Repos\\DailyProgrammerJava\\ProgramPrintsItself\\src\\PrintItself.java";
    public static void main(String[] args) throws FileNotFoundException {
        Scanner programText = new Scanner(new File(PATH));
        while (programText.hasNextLine()) {
            System.out.println(programText.nextLine());
        }
    }
}
