import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Random;

/**
 * Created by Joseph on 6/25/2014.
 *
 * @author Joseph
 */
public class TestDataGenerator {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner console = new Scanner(System.in);
        System.out.println("Records to generate: ");
        int generate = console.nextInt();
        System.out.println("Tests per student: ");
        int test = console.nextInt();
        console.close();

        String[] firstList = firstList("D:\\Users\\Joseph\\Documents\\repos\\DailyProgrammerJava\\C0168_Easy\\src\\top_names_since_1950.csv");
        String[] lastList = lastList("D:\\Users\\Joseph\\Documents\\repos\\DailyProgrammerJava\\C0168_Easy\\src\\dist.all.last");

        Student[] studentList = new Student[generate];
        for (int i = 0; i < generate; i++) {
            studentList[i] = new Student(firstList, lastList, test);
            System.out.println(studentList[i].toString());
        }
    }

    public static String[] firstList(String firstNamePath)
            throws FileNotFoundException {
        Random random = new Random();
        Scanner firstNameScanner = new Scanner(new File(firstNamePath));

        // count total lines in name file
        int lineCount = countLines(firstNameScanner) - 1; // skipping first line
        firstNameScanner = new Scanner(new File(firstNamePath));  // reopen scanner at beginning after reading length
        firstNameScanner.nextLine(); // first line of file is the guide

        // add names to string list
        String[] firstList = new String[lineCount * 2];  // 2 names per line (M/F)
        for (int i = 0; i < (lineCount * 2); i += 2) {
            Scanner line = new Scanner(firstNameScanner.nextLine());
            line.useDelimiter(","); // line is comma (",") separated
            line.next();
            line.next(); // skip to 3rd position (male name)

            String male = line.next();
            String female = line.next();

            firstList[i] = male;
            firstList[i + 1] = female;

            line.close();
        }

        return firstList;
    }

    public static String[] lastList(String lastNamePath)
            throws FileNotFoundException {
        Random random = new Random();
        Scanner lastNameScanner = new Scanner(new File(lastNamePath));

        // count total lines in last name file
        int lineCount = countLines(lastNameScanner);
        lastNameScanner = new Scanner(new File(lastNamePath)); // reopen scanner at beginning after reading length

        // add names to string list
        String[] lastList = new String[lineCount]; // one name per line
        for (int i = 0; i < lineCount; i++) {
            Scanner line = new Scanner(lastNameScanner.nextLine());

            String surname = line.next(); // surname is first word of line

            lastList[i] = surname.substring(0,1).toUpperCase() + surname.substring(1).toLowerCase();

            line.close();
        }

        return lastList;
    }

    private static int countLines(Scanner input) {
        int i = 0;
        while (input.hasNextLine()) {
            i++;
            input.nextLine();
        }
        input.close();
        return i;
    }
}
