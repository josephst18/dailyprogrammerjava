import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Joseph on 6/27/2014.
 *
 * @author Joseph Stahl
 */
public class Student {
    private String first;
    private String last;
    private int[] grades;
    private int numberTests;
    private Random random = new Random();
    private String firstNamePath = "D:\\Users\\Joseph\\Documents\\repos\\DailyProgrammerJava\\C0168_Easy\\src\\top_names_since_1950.csv";

    public Student() throws FileNotFoundException {
        first = "N/A";
        last = "N/A";
        grades = generateGrades(5);
    }

    public Student(String[] firstList, String[] lastList, int numberTests) throws FileNotFoundException {
        first = generateFirst(firstList);
        last = generateLast(lastList);
        grades = generateGrades(numberTests);
    }

    private String generateFirst(String[] firstList) {
        return (firstList[random.nextInt(firstList.length)]);
    }

    private String generateLast(String[] lastList) {
        return (lastList[random.nextInt(lastList.length)]);
    }

    private int[] generateGrades(int quantity) {
        int[] grades = new int[quantity];
        for (int i = 0; i < quantity; i++) {
            grades[i] = random.nextInt(100);
        }
        return grades;
    }

    public String toString() {
        String gradeString = "";
        for (int i : grades) {
            gradeString += i;
            gradeString += " ";
        }
        return first + ", " + last + "\t" + gradeString;
    }
}
