package C0165_Easy;

/**
 * Created by Joseph on 6/18/2014.
 *
 * Format of dual arrays (grid[][]) based on code
 * by TSLRed (http://www.reddit.com/user/TSLRed).
 *
 *
 * Lessons learned:
 *  * Using multi-dimensional arrays (i.e. int[][] intArray = new int[X][Y])
 *      Left index determines # rows; right index determines # columns
 *      !!  This is REVERSE of what is expected (Cartesian coordinates are
 *          listed (X,Y); multidimensional array is listed [Y][X].
 *  * Finding the length of a multi-dimensional array:
 *      System.out.println(grid.length); // number of rows
 *      System.out.println(grid[0].length); // number of columns in row 0
 *  * RTFM!  Don't waste time refactoring all array references because
 *      X and Y were transposed (as well as i & j and all other coordinates).
 *
 * @author Joseph Stahl
 */

import java.util.Scanner;
import java.io.*;

public class ASCIIGameOfLife {
    private static int X;
    private static int Y;
    private static int ITERATIONS;

    public static void main(String[] args)
            throws FileNotFoundException, UnsupportedEncodingException {
        System.out.println("Provide an input file: ");
        Scanner console = new Scanner(System.in);
        File inputFile = new File(console.nextLine());
        Scanner input = new Scanner(inputFile);

        ITERATIONS = input.nextInt();
        X = input.nextInt();
        Y = input.nextInt();
        // discard new line character
        input.nextLine();

        // make new grid for simulation
        char[][] grid = new char[Y][X];

        // populate grid
        grid = populateGrid(input, grid);
        input.close();

        // run simulation
        for (int i = 0; i < ITERATIONS; i++) {
            grid = roundSimulate(grid);
        }

        // print results
        System.out.printf("Result for %d rounds on a %dx%d grid: \n", ITERATIONS, X, Y);
        for (char[] cArray: grid) {
            System.out.println(cArray);
        }

        // Ask to save results
        writeResults(grid, console);
    }

    /**
     * Simulate a single round of Conway's Game Of Life.  For every cell in the
     * grid, the population of surrounding cells is counted.  If there are < 2
     * surrounding cells, the center cell will die from underpopulation
     * (becomes a '.').  If exactly 3, the cell reproduces (becomes a '#').  If
     * greater than 3, the cell dies from overpopulation (becomes a '.').
     *
     * @param grid  coordinate system of cells, noted [Y][X] where '#' is
     *              living and '.' is dead
     * @return      cell grid after one simulation
     */
    private static char[][] roundSimulate(char[][] grid) {
        char[][] tempGrid = new char[grid.length][grid[0].length];
        for (int j = 0; j < grid.length; j++) { // go line by line (Y)
            for (int i = 0; i < grid[0].length; i++) { // character by character (X)
                int surroundingPopulation = countPopulation(grid, i, j); // j is y coordinate, i is x coordinate
                if (surroundingPopulation < 2) {
                    tempGrid[j][i] = '.';
                } else if (surroundingPopulation == 3) {
                    tempGrid[j][i] = '#';
                } else if (surroundingPopulation > 3) {
                    tempGrid[j][i] = '.';
                } else {
                    tempGrid[j][i] = grid[j][i];
                }
            }
        }
        return tempGrid;
    }

    /**
     * Count population in the surrounding cells of a provided cell with
     * coordinates grid[j][i], where j is y coordinate of cell and i is
     * x coordinate of cell.
     *
     * @param grid  array containing '#' (alive) and '.' (dead) cells in array j
     *              cells tall and i cells wide
     * @param i     x coordinate of cell to count surrounding population
     * @param j     y coordinate of cell to count surrounding population
     * @return      count of surrounding # (alive) cells
     */
    private static int countPopulation(char[][] grid, int i, int j) {
        int population = 0;
        int left = checkHorizontalOverflow(i-1);
        int right = checkHorizontalOverflow(i+1);
        int above = checkVerticalOverflow(j-1);
        int below = checkVerticalOverflow(j+1);

        // left (one left of cell i, same j)
        if (grid[j][left] == '#') population++;
        // right (one right of cell i, same j)
        if (grid[j][right] == '#') population++;
        // above (same i, one above j)
        if (grid[above][i] == '#') population++;
        // below (same i, one below j)
        if (grid[below][i] == '#') population++;
        // top left (one left of i, one above j)
        if (grid[above][left] == '#') population++;
        // top right (one right of i, one above j)
        if (grid[above][right] == '#') population++;
        // bottom left (one left of i, one below j)
        if (grid[below][left] == '#') population++;
        // bottom right (one right of i, one below j)
        if (grid[below][right] == '#') population++;

        return population;
    }

    /**
     * Any x coordinate that overflows its boundaries in the grid will be
     * wrapped to the next boundary.  For example, going one cell left of a cell
     * in column 0 will return the rightmost cell of the grid rather than
     * returning a cell in column -1.
     * @param check the coordinate of the cell for checking
     * @return      input coordinate if no overflow, otherwise a wrapped coordinate
     */
    private static int checkHorizontalOverflow(int check) {
        if (check < 0) {
            return X - 1; // grid starts at 0, so max value is at position X-1
        } else if (check >= X) {
            return 0;
        } else {
            return check;
        }
    }

    /**
     * Any y coordinate that overflows its boundaries in the grid will be
     * wrapped to the next boundary.  For example, one cell above a cell in
     * row 0 will return the bottom cell of the grid rather than returning a
     * cell in row -1.
     *
     * @param check the coordinate of the cell for checking
     * @return      input coordinate if no overflow, otherwise a wrapped coordinate
     */
    private static int checkVerticalOverflow(int check) {
        if (check < 0) {
            return Y - 1; // grid starts at 0, so max value is at position Y-1
        } else if (check >= Y) {
            return 0;
        } else {
            return check;
        }
    }

    /**
     * Populate grid with the cells ('#' = living / '.' = dead) in the
     * input file.  This forms the basis for Conway's Game of Life simulation.
     *
     * @param input input scanner for reading lines from the input file
     * @param grid  grid to output characters from the input file
     * @return      fully populated grid with '#' and '.'
     */
    private static char[][] populateGrid(Scanner input, char[][] grid) {
        for (int j = 0; j < Y; j++) {
            String line = input.nextLine();
            for (int i = 0; i < X; i++) {
                grid[j][i] = line.charAt(i);
            }
        }
        return grid;
    }

    /**
     * Ask user to write results to a file.  This makes a diff easy to ensure
     * accuracy of result.
     * @param grid  grid holding the resulting Game of Life grid
     * @param console   ask user for to write file and which filename to use
     * @throws FileNotFoundException    user entered an invalid filename
     * @throws UnsupportedEncodingException encoding not supported on this platform
     */
    private static void writeResults(char[][] grid, Scanner console)
            throws FileNotFoundException, UnsupportedEncodingException {
        System.out.println("Do you want to save the results? (Y|N) ");
        String answer = console.nextLine();
        if (answer.substring(0,1).equalsIgnoreCase("Y")) {
            System.out.println("Enter a filename (any existing file will be " +
                    "overwritten): ");
            String fileName = console.nextLine();
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");
            writer.printf("Result for %d rounds on a %dx%d grid: \n", ITERATIONS, X, Y);
            for (char[] cArray : grid) {
                writer.println(cArray);
            }
            System.out.println("Done writing");
            writer.close();
        } else {
            System.out.println("Skipped writing");
        }
    }
}
