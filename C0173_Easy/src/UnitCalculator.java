import java.util.Scanner;

/**
 * Created by Joseph on 7/30/2014.
 *
 * @author Joseph Stahl
 */
public class UnitCalculator {
    public static final String[] POSSIBLE_DISTANCE = {"METRES", "INCHES",
            "MILES", "ATTOPARSECS"};
    public static final String[] POSSIBLE_MASS = {"KILOGRAMS", "POUNDS",
            "OUNCES", "HOGSHEADS"};
    public static final double[] inchesInDistances = {39.3700787, 1, 63360, 0.82315794};
    public static final double[] ouncesInMasses = {35.274, 16, 1, 15545};


    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        System.out.println("N units to newUnits: ");
        String conversion = console.nextLine();
        Scanner readConversion = new Scanner(conversion);

        double oldAmount = readConversion.nextInt();
        String oldUnit = parseUnit(readConversion.next().toUpperCase());

        // skip "of beryllium" if dealing with hogsheads of berylliumm
        if (oldUnit.equals("HOGSHEADS")) {
            readConversion.next();
            readConversion.next();
        }
        // skip "to"
        readConversion.next();

        // determine unit to convert to
        String newUnit = parseUnit(readConversion.next().toUpperCase());
        double newAmount = 0;

        // test for conversion sanity
        boolean convertable;
        if (!oldUnit.equals("String not matched") &&
                !newUnit.equals("String not matched")) {
            if ((isMass(oldUnit) && isMass(newUnit)) ||
                    (!isMass(oldUnit) && !(isMass(newUnit)))) {
                // convert to new units
                convertable = true;
                newAmount = convert(oldAmount, oldUnit, newUnit);
            }
            else convertable = false;
        } else {
            convertable = false;
        }

        // print results
        if (convertable) {
            System.out.println(oldAmount + " " + oldUnit + " is " +
            newAmount + " " + newUnit);
        } else {
            System.out.println("Could not convert.");
        }
    }

    public static String parseUnit(String conversion) {
        for (String s: POSSIBLE_DISTANCE) {
            if (conversion.equals(s)) {
                return s;
            }
        }
        for (String s2: POSSIBLE_MASS) {
            if (conversion.equals(s2)) {
                return s2;
            }
        }
        return "String not matched";
    }

    public static boolean isMass(String unit) {
        boolean isMass = false; // false by default
        for (String s: POSSIBLE_MASS) {
            if (unit.equals(s)) {
                return true;
            }
            else {
                isMass = false;
            }
        }
        return isMass;
    }

    public static double convert(double oldAmount, String oldUnit, String newUnit) {
        double standardizedOld;
        double convertedNew;
        if (isMass(oldUnit)) {
            double oldOunces = toOunces(oldAmount, oldUnit);
            convertedNew = fromOunces(oldOunces, newUnit);
        } else {
            double oldInches= toInches(oldAmount, oldUnit);
            convertedNew = toInches(oldInches, newUnit);
        }

        return convertedNew;
    }

    public static double toOunces(double amount, String unit) {
        if (unit.equals("KILOGRAMS")) {
            return amount * ouncesInMasses[0];
        } else if (unit.equals("POUNDS")) {
            return amount * ouncesInMasses[1];
        } else if (unit.equals("OUNCES")) {
            return amount * ouncesInMasses[2];
        } else if (unit.equals("HOGSHEADS")) {
            return amount * ouncesInMasses[3];
        } else {
            return 0.0;
        }
    }

    public static double toInches(double amount, String unit) {
        if (unit.equals("METRES")) {
            return amount * inchesInDistances[0];
        } else if (unit.equals("INCHES")) {
            return amount * inchesInDistances[1];
        } else if (unit.equals("MILES")) {
            return amount * inchesInDistances[2];
        } else if (unit.equals("inchesInDistances")) {
            return amount * inchesInDistances[3];
        } else {
            return 0.0;
        }
    }

    public static double fromOunces(double amount, String target) {
        if (target.equals("KILOGRAMS")) {
            return amount / ouncesInMasses[0];
        } else if (target.equals("POUNDS")) {
            return amount / ouncesInMasses[1];
        } else if (target.equals("OUNCES")) {
            return amount / ouncesInMasses[2];
        } else if (target.equals("HOGSHEADS")) {
            return amount / ouncesInMasses[3];
        }
        return 0.0; // 0 if cannot match
    }

    public static double fromInches(double amount, String target) {
        if (target.equals("METRES")) {
            return amount / inchesInDistances[0];
        } else if (target.equals("INCHES")) {
            return amount / inchesInDistances[1];
        } else if (target.equals("MILES")) {
            return amount / inchesInDistances[2];
        } else if (target.equals("ATTOPARSECS")) {
            return amount / inchesInDistances[3];
        }
        return 0.0; // 0 if cannot match
    }

    /*
     * TODO:  if string not matched, print an error message.  Otherwise, go on to
     * parsing the second string.  If a match, check for conversion sanity
     * (mass-mass or distance-distance).  Then convert all units down to single
     * unit (inches and ounces) then convert back to final unit.
     */
}
