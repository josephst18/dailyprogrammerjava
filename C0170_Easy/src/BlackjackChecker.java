import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


/**
 * TODO:
 * Also, implement blackjack checking method.  First see for 5-card
 * trick/ tie, then go to checking hand values.  If a bust & hand
 * contains ace, then decrease value of hand by ten and see if still
 * a bust.  If still a bust, check for another ace and decrease by ten
 * again, etc.
 * This will require adding a private variable and public method to
 * Player to count the number of aces in their hand (based on times
 * an ace came up while adding to the Card[] cards array.
 *
 * Keep on removing aces until the hand is at/ under 21 then store this
 * value.  Compare values between all players and print the winner!
 */

/**
 * Created by Joseph on 7/9/2014.
 *
 * @author Joseph Stahl
 */
public class BlackjackChecker {
    public static final String inputPath = "D:\\Users\\Joseph\\Documents\\repos\\DailyProgrammerJava\\C0170_Easy\\src\\exInput1.txt";

    public static void main(String[] args) throws FileNotFoundException {
        // import players
        Scanner scanner = new Scanner(new File(inputPath));
        int numberPlayers = scanner.nextInt(); // file begins with # players
        scanner.nextLine();

        // process players into array
        Player[] players = new Player[numberPlayers];
        for (int i = 0; i < players.length; i++) {
            players[i] = new Player(scanner.nextLine());
        }

        // check point values and decrement aces as necessary
        for (Player player: players) {
            while (player.getPoints() > 21 && player.getAceCount() > 0) {
                player.lowerAce();
            }
            if (player.getPoints() > 21) {
                player.setBust(true);
            }
        }

        // check for 5-card tricks on non-busted players
        boolean fivePointWinnerExists = false;
        for (Player player: players) {
            if (!player.isBust() && player.getCardCount() == 5) {
                player.setFiveCardTrick(true);
                fivePointWinnerExists = true;
            }
        }

        // total points if there are no five card tricks
        if (!fivePointWinnerExists) {
            checkPointWinner(players);
        }

        // decide on a winner or a tie
        Player[] winners = decideWinner(players, fivePointWinnerExists);

        // print winners
        printWinners(winners);

    }

    /**
     * Check which player had the highest number of points without busting
     * @param players   Player[] array of players playing  blackjack
     */
    public static void checkPointWinner(Player[] players) {
        // check what highest point value in game is
        int maxPoints = 0;
        for (Player player: players) {
            if (player.getPoints() > maxPoints && !player.isBust()) {
                maxPoints = player.getPoints();
            }
        }
        assert maxPoints <= 21; // make sure that no >21 values are winners

        // players with this many points are point winners
        for (Player player: players) {
            assert player.getPoints() <= maxPoints;
            if (player.getPoints() == maxPoints) {
                player.setPointWinner(true);
            }
        }
    }

    /**
     * Decide on a winner of the game.  Check for five-card tricks first and
     * resort to checking point values if there is no five-card trick winner.
     * @param players   Player[] array of players to check wins
     * @return          array of winners (length >1 if tie)
     */
    public static Player[] decideWinner(Player[] players,
                                        boolean fivePointWinnerExists) {
        int numWinners = 0;

        // count winners by 5-card trick
        if (fivePointWinnerExists) {
            for (Player player : players) {
                if (player.isFiveCardTrick()) {
                    numWinners++;
                }
            }
        }
        // if not 5-card trick, count point winners
        if (!fivePointWinnerExists) {
            for (Player player: players) {
                if (player.isPointWinner()) {
                    numWinners++;
                }
            }
        }

        /*
        * add five card trick winners to array if there are any,
        * otherwise add point winners
        */
        Player[] winners = new Player[numWinners];
        if (fivePointWinnerExists) {
            int winnerPosition = 0;
            for (Player player: players) {
                if (player.isFiveCardTrick()) {
                    winners[winnerPosition] = player;
                    winnerPosition++;
                }
            }
        }
        else {
            int winnerPosition = 0;
            for (Player player: players) {
                if (player.isPointWinner()) {
                    winners[winnerPosition] = player;
                    winnerPosition++;
                }
            }
        }
        return winners;
    }

    /**
     * Print winners of the game.  If the game was won with a five-card trick,
     * print a special message.  Otherwise, print the point-winner's name.
     * If multiple wins, a tie results.  If no one wins, everyone must have
     * busted.
     * @param winners   array of players who have won the game (typically just 1)
     */
    public static void printWinners(Player[] winners) {
        // check for ties
        if (winners.length > 1) {
            System.out.println("Tie");
        } else if (winners.length == 0) {
            System.out.println("Everyone busted");
        } else {
            if (winners[0].isFiveCardTrick()) {
                System.out.println(winners[0].getName() + " has won with a " +
                        "five card trick!");
            } else {
                System.out.println(winners[0].getName() + " has won!");
            }
        }
    }
}
