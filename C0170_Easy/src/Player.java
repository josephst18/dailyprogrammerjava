import java.util.Arrays;
import java.util.Scanner;

/**
 * Blackjack player class.  Stores a player's name, the number of cards in
 * their hard (useful for 5-card tricks), the value and suit of each card
 * in their hand, and the number of aces in their hand.
 * Created by Joseph on 7/9/2014.
 *
 * @author Joseph Stahl
 */
public class Player {
    private String name;
    private int cardCount = 0;
    private Card[] cards;
    private int aceCount = 0;
    private int points = 0;
    private boolean bust = false;
    private boolean fiveCardTrick = false;
    private boolean pointWinner = false;

    public Player() {
        /**
         * Default player construct
         */
        name = "undefined";
        cardCount = -1;
    }

    public Player(String playerInfo) {
        /**
         * Construct a player Object with given information.
         * Format:  "Alice: Ace of Diamonds, Ten of Clubs".  Name will be set
         * and card list processed by Card class.
         */
        Scanner playerScanner = new Scanner(playerInfo);
        playerScanner.useDelimiter(":"); // name is separated by colon (":")
        name = playerScanner.next();
        String cardString = playerScanner.nextLine().trim().substring(2); // remove pesky beginning colon (:) and space ( ).

        Scanner cardScanner = new Scanner(cardString);
        cardScanner.useDelimiter(", "); // cards are comma separated

        // count cards to know how large to make array
        while (cardScanner.hasNext()) {
            cardCount++;
            cardScanner.next();
        }
        cardScanner.close();

        // reopen card scanner to read cards into array
        cardScanner = new Scanner(cardString);
        cardScanner.useDelimiter(", ");
        cards= new Card[cardCount];
        for (int i = 0; i < cards.length; i++) {
            cards[i] = new Card(cardScanner.next());
            /*
            track aces so that they can be subtracted (i.e. valued at 1
            instead of 10) in case of bust
            */
            if (cards[i].getValue() == 11) {
                aceCount++;
            }
        }

        calculatePoints();
    }

    /**
     * Get name of player
     * @return player name
     */
    public String getName() {
        return name;
    }

    /**
     * Get number of cards in player's hand
     * @return
     */
    public int getCardCount() {
        return cardCount;
    }

    /**
     * Convert player to string by printing name and then relying on
     * Arrays.toString() to convert the cards in the player's hand into a
     * string.  Result retains [square brackets] so it is not well suited for
     * printing to user but is acceptable for debugging.
     * @return  player's name and cards in hand (value & suit)
     */
    public String toString() {
        return getName() + ": " + Arrays.toString(cards);
    }

    /**
     * Get number of aces in player's hand.  In case of a bust, up to all aces
     * in hand can be counted as 1 instead of 10 to get under 21-point limit.
     * @return  number of aces in hand
     */
    public int getAceCount() {
        return aceCount;
    }

    /**
     * Calculate point value of cards held by player
     */
    private void calculatePoints() {
        for (Card card: cards) {
            points += card.getValue();
        }
    }

    /**
     * Count an ace as 1 point instead of 11 (i.e. 10 points fewer).  This
     * method will also decrement the number of aces remaining that can be
     * lowered.
     */
    public void lowerAce() {
        assert aceCount > 0;
        points -= 10;
    }

    /**
     * Get number of points currently in player's hand
     * @return  positive integer point value of cards
     */
    public int getPoints() {
        return points;
    }

    /**
     * If player has busted after checking cards and decrementing aces
     * (if possible), set their bust variable to true.
     * @param b true if player has busted
     */
    public void setBust(boolean b) {
        bust = b;
    }

    /**
     * See if player has busted.  A player who has busted cannot have a
     * 5-card trick.
     * @return  true if busted
     */
    public boolean isBust() {
        return bust;
    }

    /**
     * Set 5-card trick to provided boolean (true if five-card trick)
     * @param b five card trick boolean
     */
    public void setFiveCardTrick(boolean b) {
        fiveCardTrick = b;
    }

    /**
     * Check whether player has a five-card trick
     * @return  true if player has a five-card trick
     */
    public boolean isFiveCardTrick() {
        return fiveCardTrick;
    }

    /**
     * Select a player as a point winner
     * @param b true if player is point winner
     */
    public void setPointWinner(boolean b) {
        pointWinner = b;
    }

    /**
     * Check whether player is a point winner
     * @return  true if player is a point winner
     */
    public boolean isPointWinner() {
        return pointWinner;
    }
}
