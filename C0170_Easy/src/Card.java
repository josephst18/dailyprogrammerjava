import java.util.Scanner;

/**
 * Simple card class.  Cards are specified in format "Ace of Spaces" or
 * "Three of Diamonds".  Case does not matter but numbered cards must be
 * provided in English (one, two, three, etc) rather than digits (1, 2, 3).
 *
 * Created by Joseph on 7/9/2014.
 *
 * @author Joseph Stahl
 */
public class Card {
    private int value;
    private String suit;

    /**
     *
     * Value is 0 and suit is undefined if string not provided.
     */
    public Card() {
        value = 0;
        suit = "undefined";
    }

    /**
     * Create a card from a provided string (i.e. "Ten of Hearts).  Value and
     * suit are parsed from the provided string.
     * @param cardName  string in format "ten of hearts" or similar
     */
    public Card(String cardName) {
        Scanner scanner = new Scanner(cardName);
        value = parseValue(scanner);
        suit = parseSuit(scanner);
    }

    /**
     * Parse value of card (1-10 for numbered cards & face cards, 11 for ace) by
     * matching string in case insensitive manner.
     * @param scanner   scanner with card text as format "$VALUE of $FACE"
     * @return          blackjack value of card, 1-11
     */
    private int parseValue(Scanner scanner) {
        String valueString = scanner.next();

        if (valueString.equalsIgnoreCase("two")) {
            return 2;
        } else if (valueString.equalsIgnoreCase("three")) {
            return 3;
        } else if (valueString.equalsIgnoreCase("four")) {
            return 4;
        } else if (valueString.equalsIgnoreCase("five")) {
            return 5;
        } else if (valueString.equalsIgnoreCase("six")) {
            return 6;
        } else if (valueString.equalsIgnoreCase("seven")) {
            return 7;
        } else if (valueString.equalsIgnoreCase("eight")) {
            return 8;
        } else if (valueString.equalsIgnoreCase("nine")) {
            return 9;
        } else if (valueString.equalsIgnoreCase("ten")) {
            return 10;
        } else if (valueString.equalsIgnoreCase("jack")) {
            return 10;
        } else if (valueString.equalsIgnoreCase("queen")) {
            return 10;
        } else if (valueString.equalsIgnoreCase("king")) {
            return 10;
        } else if (valueString.equalsIgnoreCase("ace")) {
            return 11;
        }

        return -1; // indicates an error in if/ else loop
    }

    /**
     * Parse suit of card by looking two words after the card value
     * @param scanner   scanner with card text as format "$VALUE of $FACE"
     * @return          suit of card (heart, diamond, club, space)
     */
    private String parseSuit(Scanner scanner) {
        scanner.next(); // skip "of"
        return scanner.next().toLowerCase();
    }

    /**
     * Get value of card
     * @return  value of card, 1-11
     */
    public int getValue() {
        return value;
    }

    /**
     * Get suit of card
     * @return  suit of card (heart, diamond, club, space)
     */
    public String getSuit() {
        return suit;
    }

    /**
     * Convert card to string.  Useful for debugging and testing
     * @return  string in format "value: $VALUE, suit: $SUIT"
     */
    public String toString() {
        return "value: " + getValue() + ", suit: " + getSuit();
    }
}
