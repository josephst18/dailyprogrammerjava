package dailyProgrammerPlanetaryGravity;

/**
 * Created by Joseph on 6/15/2014.
 *
 * @author Joseph
 */

import java.util.Scanner;
import java.io.*;

public class calculatePlanetGravity {
    private static final String INFILENAME = "D:\\Users\\Joseph\\Documents\\repos\\cs101\\summer_practice\\src\\dailyProgrammerPlanetaryGravity\\inputfile.txt";
    private static final String OUTFILENAME = "D:\\Users\\Joseph\\Documents\\repos\\cs101\\summer_practice\\src\\dailyProgrammerPlanetaryGravity\\outputfile.txt";
    private static final double GCONSTANT = 6.67 * Math.pow(10.0, -11);

    public static void main(String[] args) throws FileNotFoundException {
        File inputFile = new File(INFILENAME);
        Scanner inputScanner = new Scanner(inputFile);
        File outputFile = new File(OUTFILENAME);
        PrintStream output = new PrintStream(outputFile);

        int objectMass = inputScanner.nextInt();
        inputScanner.nextLine(); // discard new line character

        int numberPlanets = inputScanner.nextInt();
        inputScanner.nextLine(); // discard new line character


        while (inputScanner.hasNextLine()) {
            Scanner planetLine = new Scanner(inputScanner.nextLine());

            String name = planetLine.next();
            name = stripLastChar(name); // remove comma

            String radString = planetLine.next();
            int radius = Integer.parseInt(stripLastChar(radString));

            String denString = planetLine.next();
            // do not need to strip new line character after density string
            int density = Integer.parseInt(denString);

            Planet current = new Planet(name, radius, density);
//            System.out.println(current.getRadius() + ", " + current.getDensity()); // DEBUG
            double force = GCONSTANT * (objectMass * current.getMass()) /
                    Math.pow(current.getRadius(), 2);
            output.printf("%s: %.3f \n", current.getName(), force);
            planetLine.close();
        }
        inputScanner.close();
    }

    /**
     * Strip the last character from a string.  Useful for taking commas off
     * strings for pretty printing of planet name and use in force calculation.
     * If the string does not end with a comma, the original string is
     * returned.
     *
     *
     * @param input string with a trailing comma
     * @return      string without trailing comma
     */
    public static String stripLastChar(String input) {
        if (input.charAt(input.length() - 1) == ',') {
            return input.substring(0, input.length() - 1);
        } else {
            return input;
        }
    }
}
