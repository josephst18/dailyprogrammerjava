package dailyProgrammerPlanetaryGravity;

/**
 * Planet class for use in /r/dailyProgramming example for June 15
 * (calculatePlanetGravity.java).  Creates a planet object that determines a
 * planet's mass given its radius and density.
 *
 * Created by Joseph on 6/15/2014.
 *
 * @author Joseph
 */

public class Planet {
    private String name;
    private int radius;
    private int density;
    private double mass;

    /**
     * Planet default constructor creates planet named "undefined" with
     * a negative radius and density.  Mass is calculated from these
     * parameters.
     */
    public Planet() {
        this.name = "undefined";
        this.radius = -1;
        this.density = -1;
        setMass(getRadius(), getDensity());
    }

    /**
     * Create a planet given a planet's name, radius, and density.  Mass is
     * calculated from these parameters.
     *
     * @param name      name of the planet
     * @param radius    radius of planet
     * @param density   density of planet
     */
    public Planet(String name, int radius, int density) {
        this.name = name;
        this.radius = radius;
        this.density = density;
        setMass(getRadius(), getDensity());
    }

    public void setName(String name) {
        name = name;
    }
    public void setRadius(int radius) {
        radius = radius;
    }
    public void setDensity(int density) {
        density = density;
    }
    public void setMass(int radius, int density) {
        mass = (double)4/3 * Math.PI *
                Math.pow(getRadius(), 3) * getDensity();
    }

    public String getName() {
        return name;
    }
    public int getRadius() {
        return radius;
    }
    public int getDensity() {
        return density;
    }
    public double getMass() {
        return mass;
    }
}
